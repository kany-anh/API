axios({
  url: "https://63bea7f8e348cb07621499d9.mockapi.io/food",
  method: "GET",
})
  .then(function (res) {
    renderFoodList(res.data);
  })
  .catch(function (err) {});

function renderFoodList(foodArr) {
  var contentHTML = "";
  foodArr.forEach(function (food) {
    var contentTr = `<tr>
                      <td>${food.maMon}</td>
                      <td>${food.tenMon}</td>
                      <td>${food.giaMon}</td>
                      <td>${food.loaiMon}</td>
                      <td>${food.hinhAnh}</td>
                      <td>
                      <button onclick='xoaMonAn(${food.maMon})' class='btn btn-danger'>Xoá
                      </button>
                      </td>
                      </tr>`;
    contentHTML += contentTr;
    console.log(contentHTML);
  });
  return (document.getElementById("tbodyFood").innerHTML = contentHTML);
}

function xoaMonAn(id) {
  axios({
    url: `https://63bea7f8e348cb07621499d9.mockapi.io/food/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      renderFoodList(res.data);
    })
    .catch(function (err) {});
}
